import React, { Component } from 'react';

class Form extends Component {
    constructor(){
        super()
        this.state={
            name :'',
            age:'',
           


        }
    } 
    handleInputChange = (event) =>{
        console.log(event)
        const name =event.target.name 
        const value = event.target.value
        this.setState({ [name] :value} )
    }     
    formOnSubmit =() =>{
        alert(`Name is ${this.state.name} and Age is ${this.state.age}`)
    }  
    render() {
        return (
            <div>
                <form onSubmit={this.formOnSubmit}>
                <label>User Name</label>
               <input  name ="name" type="text" value={this.state.name} onChange={this.handleInputChange}/>
               <label>Age</label>
               <input name ="age" type="text" value={this.state.age} onChange={this.handleInputChange}/>
               <input type="submit" value="submit"/>
                </form>
                
               
            </div>
        );
    }
}

export default Form;