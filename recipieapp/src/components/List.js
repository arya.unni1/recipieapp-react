import React,{useState,useEffect} from 'react';
import Recipe from './Recipe'
import '../App.css';
function List({match: items}) {
  const APP_ID ='81129f4f';
  const APP_KEY ='61438a23e17539bc23080eb09141af49';
  const [recipes,setrecipes] = useState([]);
  const [search,setSearch] = useState('');
  const [test,setTest] = useState('');
  console.log("data",items.params.title)
  let key=items.params.title;
  const [query,setQuery] =useState(key);

   
    
  useEffect(()=>{
   if(query!=undefined){ 
   getrecipes()
   }
  },[query])


  const getrecipes = async() =>{
      const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`)
    const data = await response.json()
    setrecipes(data.hits)
  }

  const updateSearch = e =>{
    setSearch(e.target.value)
    
  }

  const getSearch = e =>{

console.log('data',test)

    e.preventDefault();
    setQuery(search)
    setSearch('')
    
  }
 
  return (
   
    <div className="App">
      {/* inline style */}
     
     
      <form className="search-form" >
        <input className="search-text" type="text" value={search} onChange={updateSearch}/>
        {/* <input className="search-text" type="text" value={test} onChange={updateSearch}/> */}

        <button className="search-button" onClick={getSearch}>Search</button>
       
      </form>
      <div className="recipes">
      {recipes.map(recipe =>(
            
            <Recipe key ={recipe.recipe.label }
            title={recipe.recipe.label} 
            calories={recipe.recipe.calories} 
            image={recipe.recipe.image}
            ingredients ={recipe.recipe.ingredients}
            ></Recipe>
           
            
          ))}
          </div>
    </div>
  );
}

export default List;
