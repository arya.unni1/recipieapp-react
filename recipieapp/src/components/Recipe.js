import React,{useState} from 'react'
import ItemDetail from './ItemDetail'
import {Link} from 'react-router-dom';
const Recipe = (props) =>{
    const title =  props.title
    const ingredients =props.ingredients
    const calories = props.calories
    const image = props.image
    const [ingredient,setIngredient] =useState("")
    console.log(image)
const onIngredientClick = () =>
{
   const ingredientList = props.ingredients.map(ingredients =>(<li>{ingredients.text}</li>))
   setIngredient(ingredientList)
}




    return(
        <div >
                
            <h2 key={title}><Link to={`/item/${title}`}>{title}</Link></h2>
            
            {/* <ol>
                {ingredients.map(ingredients =>(<li>{ingredients.text}</li>))}
            </ol> */}
            <p>calories :{calories}</p>
            <button className="buttonClass" onClick={onIngredientClick} >Ingredients</button>
               <ol>{ingredient}</ol>
               
            <img src={image} alt=""/>
       
        </div>
     
    )
}
export default Recipe