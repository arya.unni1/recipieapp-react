import React from 'react'
import {Link} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css'
import Forms from './Form'
const Main= (props) =>{
   const ItemList=["orange","apple" ,"mango"]
       
   const ItemTag=ItemList.map(item =><li> <h4 key={item} > <Link to={`/list/${item}`}>{item.toUpperCase()}</Link></h4></li>)
    return(<div className="App">
            <ol>
          {ItemTag}

            </ol>
           <Forms></Forms>

    </div>)

}
export default Main
