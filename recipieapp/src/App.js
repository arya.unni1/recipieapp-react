import React from 'react';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import './App.css';
import List from './components/List'
import Item from './components/LifeCycle'
import Main from './components/Main'
import ItemDetail from './components/ItemDetail'
function App() {
  return( 
    

      <div className="App">
       <Router>
<h1 style={{"textAlign":"center"}}>Recipe App</h1> 
    <Route path="/" exact component={Main}/> 
    <Route path="/list" exact component={List}/>
    <Route path="/list/:title" component={List}/> 
    <Route path="/item" exact component={Item}/>
    <Route path="/item/:title" component={ItemDetail}/>
    </Router>
    </div>
    
    
  );
  
}

export default App;
